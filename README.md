# MultiplayerTemplate

This is my first introduction to replication to unreal it should work as an template to start you with replication. The examples here are pretty basic but cover most use cases of replication. The code is commented and should help you to understand replication. It is also in c++ and that is sparse to go by.

This project uses simple replication and should be build upon using newer features like GAS

Implemented stuff:
- [x] Health component with replication logic
- [x] replicated itemp pickup
- [x] projectile replication
- [x] simple GameMode & Game State replication
- [x] simple Player & Player State replication 

Usefull resources:
- https://www.youtube.com/watch?v=JOJP0CvpB8w&ab_channel=AlexForsythe


Developed with Unreal Engine 4 and ported to 5
