// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MULTIPLAYERTEMPLATE_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();
protected:

	UPROPERTY(ReplicatedUsing=OnRep_Health)
	float Health;

	UFUNCTION()
	void OnRep_Health();

	UPROPERTY(EditAnywhere, Category = "Stats")
	float MaxHealth;
		
protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	//PRC for health to server propagation
	//this declaration is implemented automaticaly by UE4
	//just need to declare Validate and Implementation
	UFUNCTION(Server,Reliable, WithValidation)
	void Server_AddHealth(float value);
	bool Server_AddHealth_Validate(float value);
	void Server_AddHealth_Implementation(float value);
	
	

public:	

	void AddHealth(float value);


	float GetHealth();
};
