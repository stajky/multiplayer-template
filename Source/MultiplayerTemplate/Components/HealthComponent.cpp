// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerTemplate/Components/HealthComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	Health = 0;
	MaxHealth = 100;
}

void UHealthComponent::OnRep_Health()
{
	//This will be called AFTER the value has been replicated
	if (Health <= 0) {
		//Here I would play the death animation and set the game over screen
		UE_LOG(LogTemp, Warning, TEXT("Play Dead animation"))
	}
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	SetIsReplicated(true);
	Health = MaxHealth;
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, Health);
}

bool UHealthComponent::Server_AddHealth_Validate(float value)
{
	return true;
}

void UHealthComponent::Server_AddHealth_Implementation(float value)
{
	AddHealth(value);
}

void UHealthComponent::AddHealth(float value)
{
	//if we are not on the server send event to change the value on server 
	if (GetOwnerRole() == ROLE_Authority){
		Health += value;
	}
	else {
		//if we are on a client we need to call and RPC so that server changes the value and then replicates it
		Server_AddHealth(value);
	}
	
}

float UHealthComponent::GetHealth()
{
	return Health;
}


