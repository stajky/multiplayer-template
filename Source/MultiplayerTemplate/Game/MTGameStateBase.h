// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "MTGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTEMPLATE_API AMTGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	AMTGameStateBase();

public:
	void PlayerHit();

protected:
	UPROPERTY(ReplicatedUsing=OnRep_TotalHits)
	uint16 TotalHits;

	UFUNCTION()
	void OnRep_TotalHits();

public:
	uint16 GetTotalHits() { return TotalHits; }
};
