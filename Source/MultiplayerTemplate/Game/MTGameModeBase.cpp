// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerTemplate/Game/MTGameModeBase.h"

#include "MTGameStateBase.h"
#include "../Player/MTPlayerState.h"



AMTGameModeBase::AMTGameModeBase() : Super() 
{

}

void AMTGameModeBase::PlayerHit()
{
    if (AMTGameStateBase* GS = GetGameState<AMTGameStateBase>()) {
        //UE_LOG(LogTemp, Warning, TEXT("Player hit game mode"));
        GS->PlayerHit();

    }

}
