// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MTGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTEMPLATE_API AMTGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AMTGameModeBase();

	void PlayerHit();
};
