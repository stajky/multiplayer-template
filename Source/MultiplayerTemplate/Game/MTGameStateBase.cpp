// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerTemplate/Game/MTGameStateBase.h"

#include "Net/UnrealNetwork.h"

AMTGameStateBase::AMTGameStateBase()
{
    TotalHits = 0;

}

void AMTGameStateBase::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AMTGameStateBase, TotalHits);
}

void AMTGameStateBase::PlayerHit()
{
    if (HasAuthority()) {
        //UE_LOG(LogTemp, Warning, TEXT("Player hit state mode"));
        TotalHits++;
        //UE_LOG(LogTemp, Warning, TEXT("SERVER: Total hits %d"), TotalHits);
    }   
}

void AMTGameStateBase::OnRep_TotalHits()
{
    UE_LOG(LogTemp, Warning, TEXT("CLIENT: Total hits %d"), TotalHits);
}
