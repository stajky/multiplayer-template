// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerTemplate/Projectile/Projectile.h"

#include "Components/SphereComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Net/UnrealNetwork.h"
#include "../Player/MTPlayer.h"
#include "../Components/HealthComponent.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"
#include "../Game/MTGameModeBase.h"
#include "../Game/MTGameStateBase.h"
#include "../Player/MTPlayerState.h"


// Sets default values
AProjectile::AProjectile()
{
	bReplicates = true;

	//Definition for the SphereComponent that will serve as the Root component for the projectile and its collision.
	SphereComponent = CreateDefaultSubobject<USphereComponent>(TEXT("RootComponent"));
	SphereComponent->InitSphereRadius(37.5f);
	SphereComponent->SetCollisionProfileName(TEXT("BlockAllDynamic"));
	RootComponent = SphereComponent;




	//Definition for the Mesh that will serve as our visual representation.
	static ConstructorHelpers::FObjectFinder<UStaticMesh> DefaultMesh(TEXT("/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere"));
	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	StaticMesh->SetupAttachment(RootComponent);

	//Set the Static Mesh and its position/scale if we successfully found a mesh asset to use.
	if (DefaultMesh.Succeeded())
	{
		StaticMesh->SetStaticMesh(DefaultMesh.Object);
		StaticMesh->SetRelativeLocation(FVector(0.0f, 0.0f, -37.5f));
		StaticMesh->SetRelativeScale3D(FVector(0.75f, 0.75f, 0.75f));
	}

	//Definition for the Projectile Movement Component.
	ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	ProjectileMovementComponent->SetUpdatedComponent(SphereComponent);
	ProjectileMovementComponent->InitialSpeed = 1500.0f;
	ProjectileMovementComponent->MaxSpeed = 1500.0f;
	ProjectileMovementComponent->bRotationFollowsVelocity = true;
	ProjectileMovementComponent->ProjectileGravityScale = 0.0f;

	static ConstructorHelpers::FObjectFinder<UParticleSystem> DefaultExplosionEffect(TEXT("/Game/StarterContent/Particles/P_Explosion.P_Explosion"));
	if (DefaultExplosionEffect.Succeeded())
	{
		ExplosionEffect = DefaultExplosionEffect.Object;
	}

	Damage = 10;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();

	//we need to register it here because the Role is not set correctly
	//Registering the Projectile Impact function on a Hit event.
	//The onhit event will be called on the server only
	//need to call get local role since the projectile will be called from the server but local is the client sometimes
	if (GetLocalRole() == ROLE_Authority) {
		SphereComponent->OnComponentHit.AddDynamic(this, &AProjectile::OnProjectileImpact);
	}
}

void AProjectile::Destroyed()
{
	FVector spawnLocation = GetActorLocation();
	UGameplayStatics::SpawnEmitterAtLocation(this, ExplosionEffect, spawnLocation, FRotator::ZeroRotator, true, EPSCPoolMethod::AutoRelease);
}

void AProjectile::OnProjectileImpact(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//WE are on a SERVER HERE
	if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {
		//we need to cast the actor to our player and call add health on its health component
		//the replication of the health is handled in the Health componanet we can just call add health how we like
		if (auto player = Cast<AMTPlayer>(OtherActor)) {
			player->GetHealthComponent()->AddHealth(-Damage);

			if (AMTGameModeBase* GM = GetWorld()->GetAuthGameMode<AMTGameModeBase>()) {
				GM->PlayerHit();
				if (AMTPlayer* ShotingPlayer = Cast<AMTPlayer>(GetOwner())) {
					if (AMTPlayerState* PS = ShotingPlayer->GetPlayerState<AMTPlayerState>()) {
						PS->PlayerHit();
					}
				}
			}
		}
	}

	Destroy();
}






