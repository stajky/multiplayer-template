// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "MTPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYERTEMPLATE_API AMTPlayerState : public APlayerState
{
	GENERATED_BODY()
public:
	AMTPlayerState();

protected:
	UPROPERTY(ReplicatedUsing=OnRep_TotalHits)
	uint16 TotalHits;


	UFUNCTION()
	void OnRep_TotalHits();

public:
	void PlayerHit();
	
};
