// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerTemplate/Player/MTPlayerState.h"
#include "../Game/MTGameStateBase.h"
#include "Net/UnrealNetwork.h"



AMTPlayerState::AMTPlayerState() 
{
    TotalHits = 0;
}

void AMTPlayerState::OnRep_TotalHits()
{
    if (AMTGameStateBase* GS = GetWorld()->GetGameState<AMTGameStateBase>()) {
        uint16 GSHits = GS->GetTotalHits();
        UE_LOG(LogTemp, Warning, TEXT("%d/%d of the total hits"), TotalHits, GSHits);
    }
}

void AMTPlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty >& OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);

    DOREPLIFETIME(AMTPlayerState, TotalHits);
}

void AMTPlayerState::PlayerHit()
{
    if (HasAuthority()) {
        //UE_LOG(LogTemp, Warning, TEXT("Player hit player state"));
        TotalHits++;
        //UE_LOG(LogTemp, Warning, TEXT("SERVER: Player hits %d"), TotalHits);
    }
}
