// Fill out your copyright notice in the Description page of Project Settings.


#include "MultiplayerTemplate/Pickups/Pickup_Health.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Net/UnrealNetwork.h"

#include "../Player/MTPlayer.h"
#include "../Components/HealthComponent.h"



// Sets default values
APickup_Health::APickup_Health()
{
    SphereRadius = 100;
    Value = 10;

    SphereComp = CreateDefaultSubobject<USphereComponent>("SphereComponent");
    SphereComp->InitSphereRadius(SphereRadius);
    SphereComp->SetCollisionProfileName("Trigger");
    RootComponent = SphereComp;

    MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
    MeshComp->SetupAttachment(RootComponent);

    SphereComp->OnComponentBeginOverlap.AddDynamic(this, &APickup_Health::OnOverlapBegin);


}

// Called when the game starts or when spawned
void APickup_Health::BeginPlay()
{
    Super::BeginPlay();
}

void APickup_Health::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
    UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) 
{
    //Server call
    //since movement is replicated on the server and Pickup_Health is also replicated the OnOverlapBegin happens deterministicaly on both the 
    //local and server at the same time -> we simply care only about the server one doe
    if (HasAuthority()) {
        if (OtherActor != nullptr && OtherActor != this && OtherComp != nullptr) {
            if (auto player = Cast<AMTPlayer>(OtherActor)) {
                Destroy();
                //we need to cast the actor to our player and call add health on its health component
                //the replication of the health is handled in the Health componanet we can just call add health how we like
                Cast<AMTPlayer>(OtherActor)->GetHealthComponent()->AddHealth(Value);
            }
        }
    }

}

